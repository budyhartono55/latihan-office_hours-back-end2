// //=======================LATIHAN BASIC JS=================================

// //0. Penulisan
//         let a;--------------> deklarasi
//         a = 5;----------------> assignment
//         let a = 5;--------------------> inisialisasi
// //1. Tipe Variable
//         var cobaVar = 5; -------> inisialisasi
//                     "sifat (Global Scope, redeclare, hoisting)"
//         let cobaLet = 1;
//                     "sifat (Block Scope, noredeclare but can update)"
//         const cobaConst = 99;
//                     "sifat (Block Scope, no redeclare can't change)"
// //=============================================================================

// //2. Coba Menampilkan part 1
// let tampil = "Hallo nama gua Asep";
// console.log(tampil);

// //part 2
// const peliharaan = "Tuyul"; //------------true
// if (peliharaan === "Tuyul") {
//   console.log(`Asep Import ${peliharaan} ke Jepang `);
// } else {
//   console.log("Asep Bangkrut");
// }

// const peliharaan = "Macan"; //------------false
// if (peliharaan === "Tuyul") {
//   console.log(`Asep Import ${peliharaan} ke Jepang `);
// } else {
//   console.log("Asep Bangkrut");
// }

// function cobaHitung(tambah1, tambah2) {
//   console.log(tambah1 + tambah2);
// }
// cobaHitung(4, 5);

// Bersambung...........................
